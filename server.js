require('./app-global');
const express = require(`${__config}/express`),
 config = require(`${__config}/config`),
 server = require('http').Server(express);
 io = require('socket.io')(server,{
    cors:{
        origin:"*"
    }
 });
const chatDetails = [];
const unreadChat = [];
const {setActiveUser,returnActiveUser,removeUser} = require("./src/utils/socketFunction");
 io.on("connection", async (socket) => {
    const dateObject = new Date();
    const time = dateObject.getHours()+":"+dateObject.getMinutes()+":"+dateObject.getSeconds();
    socket.on("addNewUser",(data)=>{
        setActiveUser(data,socket.id)
        io.emit("getUserList",returnActiveUser());
    });
    socket.on("sendMsgTo",(data)=>{

        const {user,msg,sender} = data;

        const obj = {
            msg:msg,
            reciever:user.email,
            sender:sender,
            time:time
        };

        chatDetails.push(obj);

        io.to(user.socket_id).emit("getMsg",chatDetails)
        io.to(user.socket_id).emit("checkUnread",obj);
        socket.emit("sentMsg",chatDetails);
    });
    socket.on("disconnect",()=>{
        removeUser(socket.id);
        io.emit("getUserList",returnActiveUser());
    })
});

//import db 
//require(`${__config}/dbConn`);
server.listen(`${config.port}`);
console.log(`Server is listing on port ${config.port}`)
