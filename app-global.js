global.__root = `${__dirname}`;
global.__base= `${__root}/src`;
global.__config = `${__base}/config`;
global.__routes = `${__base}/routes`;
global.__controller = `${__base}/controller`;
global.__models = `${__base}/models`