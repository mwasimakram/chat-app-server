
const UserModel = require(`${__models}/users`);

exports.authorizeUser = async (req, res) => {
    try {
        const {email,password} = req.query;
        const user = new UserModel();
        const result =await UserModel.find({email:email,password:password});
        res.send({success:true,data:result[0]});
    } catch (error) {
        console.log(error)
        res.send({ suceess: false, error: error });
    }
}
exports.userSignUp  =async (req,res) => {
    try {
        const {firstName,lastName,email,password} = req.body;
        const obj={
            firstName:firstName,
            lastName:lastName,
            email:email,
            password:password
        }
        const user = new UserModel(obj);
        const result = await user.save();
        res.send({success:true,data:result});
    } catch (error) {
        res.send({ suceess: false, error: error });
    }
}
