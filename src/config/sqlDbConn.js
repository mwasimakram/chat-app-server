const mysql = require('mysql');
const config = require('./config');
const conn = mysql.createConnection({
    host: "localhost",
    user: "yourusername",
    password: "yourpassword"
});

conn.connect((err)=>{
    if(err) console.log("[Database][Error]",err)
        console.log("Database connected");
});
