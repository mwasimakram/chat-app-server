
const express = require('express'),
    app = express(),
    cors = require('cors'),
    cookieParser = require('cookie-parser'),
    moduleName = "'wasim'",
    indexRouter = require(`${__routes}/`);
    const path = require('path');
    var engines = require('consolidate');
    const db = require("./dbConn");
app.use(cors());
app.set('view engine', 'ejs');
app.use(express.json());
app.use(express.urlencoded({extended:false}));
app.use(express.static(path.join(__dirname + '/')));

//app.use(cookieParser());

// app.use((req,res,next)=>{
//     if(!req.method.includes("POST,GET,DELETE,PUT,PATCH")) res.status(403).json({"message":"Mehtod Not allowed"});
//         next();
// })
app.use(express.static(__root + '/'));

app.get('/',(req,res)=>{
    res.render(__root+'/index.ejs',{client_id:"a97b77e90017db5a1780"});

});
app.use('/api/v1',indexRouter);

module.exports=app;