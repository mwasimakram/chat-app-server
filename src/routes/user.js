module.exports = (router,controller)=>{
   router.get('/authorize',controller.authorizeUser);
   router.post('/signUp',controller.userSignUp);
}