const { is } = require("bluebird");

let activeUser = [];
 function setActiveUser(obj={},id){
    obj.socket_id = id;
    const isExist = activeUser.findIndex(user => user.email == obj.email);
    if(isExist != -1){
        return activeUser[isExist].socket_id = id;
    }
    activeUser.push(obj);
    return activeUser;
}
 function returnActiveUser(){
    return activeUser;
}
function removeUser(id){
    const isExist = activeUser.filter(user => user.socket_id !== id);
    activeUser = isExist;
    return activeUser

}
module.exports.setActiveUser = setActiveUser;
module.exports.returnActiveUser = returnActiveUser;
module.exports.removeUser = removeUser;